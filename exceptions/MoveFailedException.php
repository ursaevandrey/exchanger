<?php

namespace exchanger\exceptions;

/**
 * Class DataNotLoadedException
 * @package exchanger\exceptions
 *
 * @author Ursaev Andrey
 */
class MoveFailedException extends \Exception
{
}