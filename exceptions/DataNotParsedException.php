<?php

namespace exchanger\exceptions;

/**
 * Class DataNotLoadedException
 * @package exchanger\exceptions
 *
 * @author Ursaev Andrey
 */
class DataNotParsedException extends \Exception
{
}