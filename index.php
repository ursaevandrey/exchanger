<?php

use exchanger\providers\{
    AlbaSData, AlbaSTransfer
};
use exchanger\core\AlbaPrestaConfig;

include_once "vendor/autoload.php";

(new exchanger\App(
    new AlbaSData(),
    new AlbaSTransfer(),
    new AlbaPrestaConfig(),
))->start();