<?php

namespace exchanger\core;

use exchanger\exceptions\DataNotLoadedException;

/**
 * Class Transfer
 * @package exchanger\core
 *
 * @author Ursaev Andrey
 */
abstract class Transfer
{

    private ?Data $data = null;

    /**
     * @return Data
     * @throws DataNotLoadedException
     */
    protected final function getData(): Data
    {
        if ($this->data === null) {
            throw new DataNotLoadedException();
        }

        return $this->data;
    }

    /**
     * @param Data $data
     */
    public function init(Data $data)
    {
        $this->data = $data;
        $this->data->parse();
    }

    /**
     * @throw MoveFailedException
     * @return bool
     */
    abstract public function move(): bool;

}