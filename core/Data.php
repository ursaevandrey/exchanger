<?php

namespace exchanger\core;

/**
 * Interface Data
 * @package exchanger\core
 *
 * @author Ursaev Andrey
 */
interface Data
{

    /**
     * Load data
     */
    public function load(LoadConfig $config): void;

    /**
     * @return void
     */
    public function parse(): void;

    /**
     * @return Product[]
     */
    public function getProducts(): array;

}