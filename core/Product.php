<?php

namespace exchanger\core;

/**
 * Class Product
 * @package exchanger\core
 *
 * @author Ursaev Andrey
 */
abstract class Product
{

    private int $id = 0;
    private string $name = '';
    private string $description = '';

    /**
     * @return int
     */
    public final function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public final function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public final function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public final function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public final function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public final function setDescription(string $description): void
    {
        $this->description = $description;
    }

}