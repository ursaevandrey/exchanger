<?php

namespace exchanger;

use exchanger\core\Data;
use exchanger\core\LoadConfig;
use exchanger\core\Transfer;

/**
 * Class App
 * @package exchanger
 *
 * @author Ursaev Andrey
 */
class App
{

    private Transfer $transfer;
    private Data $data;

    public function __construct(Data $data, Transfer $transfer, LoadConfig $config)
    {
        $this->data = $data;
        $this->transfer = $transfer;

        $this->data->load($config);
        $this->transfer->init($this->data);
    }

    public function start(): void
    {
        $this->transfer->move();
    }

}