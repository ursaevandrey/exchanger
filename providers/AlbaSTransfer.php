<?php

namespace exchanger\providers;

use exchanger\core\Transfer;

/**
 * Class AlbaSTransfer
 * @package exchanger\providers
 *
 * @method AlbaSData getData
 *
 * @author Ursaev Andrey
 */
class AlbaSTransfer extends Transfer
{

    private $foundIdsProduct = [];
    private $foundIdsCombination = [];

    public function move(): bool
    {
        $products = $this->getData()->getProducts();
        foreach ($products as $productData) {
            $prestaProduct = $this->findProduct($productData);

            if ($prestaProduct === null) {
                $prestaProduct = $this->createProduct($productData);

                foreach ($productData->getCombinations() as $combinationData) {
                    $prestaCombination = $this->createCombination($prestaProduct->id, $combinationData);
                    $this->foundIdsCombination[] = $prestaCombination->id;
                }


            } else {
                $this->updateProduct($prestaProduct, $productData);

                foreach ($productData->getCombinations() as $combinationData) {
                    $prestaCombination = $this->findCombination($combinationData);

                    if ($prestaCombination === null) {
                        $this->createCombination($prestaProduct->id, $combinationData);
                    } else {
                        $this->updateCombination($prestaCombination, $combinationData);
                    }

                    $this->foundIdsCombination[] = $prestaCombination->id;
                }
            }

            $this->foundIdsProduct[] = $prestaProduct->id;
        }

        return true;
    }

    private function findProduct(AlbaSProduct $data): ?\Product
    {
    }

    private function updateProduct(\Product $product, AlbaSProduct $data): bool
    {
    }

    private function createProduct(AlbaSProduct $data): \Product
    {
    }

    private function disableProduct(int $id): bool
    {
    }

    private function findCombination(Combination $data): ?\Combination
    {
    }

    private function updateCombination(\Combination $combination, Combination $data): bool
    {
    }

    private function createCombination(int $idProduct, Combination $data): \Combination
    {
    }

    private function disableCombination(int $id): bool
    {

    }

}