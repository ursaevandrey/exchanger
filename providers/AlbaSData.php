<?php

namespace exchanger\providers;

use exchanger\core\AlbaPrestaConfig;
use exchanger\core\Data;
use exchanger\core\LoadConfig;
use exchanger\exceptions\DataNotParsedException;
use exchanger\utils\DB;

/**
 * Class AlbaSData
 * @package exchanger\providers
 *
 * @author Ursaev Andrey
 */
class AlbaSData implements Data
{

    /** @var string[] */
    private array $items;

    /** @var string[] */
    private array $images;

    /** @var AlbaSProduct[] */
    private array $products;

    /**
     * @param AlbaPrestaConfig $config
     */
    public function load(LoadConfig $config): void
    {
        $resultProducts = DB::getInstance()->db()
            ->prepare('SELECT * FROM `ps_exc_image` WHERE `session` = ' + $config->getSessionID())
            ->fetchAll();

        foreach ($resultProducts as $resultProduct) {
            $this->items[] = $resultProduct['data'];
        }

        // TODO: Implement load() method.
    }

    public function parse(): void
    {
        $this->products = [];
        foreach ($this->items as $item) {
            $this->products[] = $this->createProduct($item);
        }
    }

    /**
     * @return AlbaSProduct[]
     * @throws DataNotParsedException
     */
    public function getProducts(): array
    {
        if ($this->products === null || empty($this->products)) {
            throw new DataNotParsedException();
        }

        return $this->products;
    }

    /**
     * @param array $data
     * @return AlbaSProduct
     */
    private function createProduct(array $data): AlbaSProduct
    {
        $product = new AlbaSProduct();

        $product->setId((int)$data['id']);
        $product->setName($data['name']);
        $product->setCode($data['kod']);
        $product->setArticleNumber($data['artikyl']);
        $product->setCategory($this->createCategory($data));

        foreach ($data['details'] as $detailData) {
            $product->addCombinations($this->createCombination($detailData));
        }
    }

    /**
     * @param array $data
     * @return Category
     */
    private function createCategory(array $data): Category
    {
        $category = new Category();
        $category->setCode($data['KodKategory']);
        $category->setName($data['NameKategory']);

        return $category;
    }

    /**
     * @param string $data
     * @return Combination
     */
    private function createCombination(string $data): Combination
    {
        $combination = new Combination();

        $combination->setCount($data['count']);
        $combination->setSize($data['size']);
        $combination->setMaterialType($data['tipMaterial']);
        $combination->setColor($data['color']);

        /** @var array $pricesData */
        $pricesData = $data['prices'];

        foreach ($pricesData as $priceData) {
            $price = new Price();
            $price->setType($priceData['tipPrice']);
            $price->setValue($priceData['price']);

            $combination->addPrice($price);
        }
    }

}