<?php

namespace exchanger\core;

/**
 * Class AlbaPrestaConfig
 * @package exchanger\core
 */
class AlbaPrestaConfig extends LoadConfig
{

    private $sessionID = 51;

    /**
     * @return int
     */
    public function getSessionID(): int
    {
        return $this->sessionID;
    }

}