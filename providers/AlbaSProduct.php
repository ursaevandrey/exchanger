<?php

namespace exchanger\providers;

use exchanger\core\Product;

/**
 * Class AlbaSProduct
 * @package exchanger\providers
 *
 * @author Ursaev Andrey
 */
class AlbaSProduct extends Product
{

    private string $code;
    private string $articleNumber;

    private Category $category;

    /** @var Combination[] */
    private array $combinations;

    /** @var string[] */
    private array $images;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getArticleNumber(): string
    {
        return $this->articleNumber;
    }

    /**
     * @param string $articleNumber
     */
    public function setArticleNumber(string $articleNumber): void
    {
        $this->articleNumber = $articleNumber;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return Combination[]
     */
    public function getCombinations(): array
    {
        return $this->combinations;
    }

    /**
     * @param Combination $combination
     */
    public function addCombinations(Combination $combination): void
    {
        $this->combinations[] = $combination;
    }

    /**
     * @return string[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param string[] $image
     */
    public function addImages(string $image): void
    {
        $this->images[] = $image;
    }

}

/**
 * Class Category
 * @package exchanger\providers
 *
 * @author Ursaev Andrey
 */
class Category
{

    private string $code;
    private string $name;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

}

/**
 * Class Image
 * @package exchanger\providers
 *
 * @author Ursaev Andrey
 */
class Image
{
}

/**
 * Class Combination
 * @package exchanger\providers
 *
 * @author Ursaev Andrey
 */
class Combination
{

    private string $count;
    private string $size;
    private string $materialType;
    private string $color;

    /** @var Price[] */
    private array $prices;

    /** @var Image[] */
    private array $images;

    /**
     * @return string
     */
    public function getCount(): string
    {
        return $this->count;
    }

    /**
     * @param string $count
     */
    public function setCount(string $count): void
    {
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize(string $size): void
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getMaterialType(): string
    {
        return $this->materialType;
    }

    /**
     * @param string $materialType
     */
    public function setMaterialType(string $materialType): void
    {
        $this->materialType = $materialType;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return Price[]
     */
    public function getPrices(): array
    {
        return $this->prices;
    }

    /**
     * @param Price $price
     */
    public function addPrice(Price $price): void
    {
        $this->prices[] = $price;
    }

    /**
     * @return Image[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param Image $image
     */
    public function addImage(Image $image): void
    {
        $this->images[] = $image;
    }

}

/**
 * Class Price
 * @package exchanger\providers
 *
 * @author Ursaev Andrey
 */
class Price
{

    private string $type;
    private string $value;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

}