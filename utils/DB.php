<?php

namespace exchanger\utils;

/**
 * Class AlbaSData
 * @package exchanger\providers
 *
 * @author Ursaev Andrey
 */
class DB
{

    /** @var DB */
    private static $instance;

    /** @var \PDO */
    private $db;

    private function __construct($dsn, $user, $password)
    {
        $this->db = new \PDO($dsn, $user, $password);
    }

    /**
     * @return DB
     */
    public static function getInstance(): DB
    {
        if (self::$instance === null) {
            self::$instance = new DB('', '', '');
        }

        return self::$instance;
    }

    /**
     * @return \PDO
     */
    public function db(): \PDO
    {
        return $this->db;
    }

}